import Dashboard from "views/Dashboard/Dashboard";
import TweetMap from "views/TweetMap/TweetMap";
import Statistics from "views/Statistics/Statistics";
import Download from "../views/Download/Download";
import About from "../views/About/About";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  {
    path: "/map",
    name: "Twitter map",
    icon: "pe-7s-map",
    component: TweetMap
  },
  {
    path: "/statistics",
    name: "Statistics",
    icon: "pe-7s-display1",
    component: Statistics
  },
  {
    path: "/download",
    name: "Download",
    icon: "pe-7s-cloud-download",
    component: Download
  },
  {
    path: "/about",
    name: "About",
    icon: "pe-7s-info",
    component: About
  },
  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default dashboardRoutes;
