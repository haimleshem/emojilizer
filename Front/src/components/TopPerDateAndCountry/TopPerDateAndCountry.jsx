import React, { Component } from "react";
import ReactDOM from "react-dom";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import "emoji-mart/css/emoji-mart.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Select from "react-select";
import "./style.css";
import { Loader, Button } from "semantic-ui-react";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

class TopPerDateAndCountry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      isDateSelected: false,
      isCountrySelected: false,
      data: [],
      selectedCountry: null,
      isLoaded: false,
      isLoading: false
    };
    this.getData = this.getData.bind(this);
  }

  handleDateChange(date) {
    this.setState({
      startDate: date,
      isDateSelected: true
    });
  }

  handleCountryChange(newValue) {
    this.setState({
      selectedCountry: newValue.value,
      isCountrySelected: true
    });
  }

  getData() {
    this.setState({ isLoading: true, isLoaded: false });
    fetch("http://emojilizer.tk:8080/api/search_date_location", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        date: this.state.startDate.toISOString(),
        country: this.state.selectedCountry
      })
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.setState(
          {
            data: json.top,
            isLoaded: true,
            isLoading: false
          },
          () => this.ParseResponse()
        );
      });
  }

  GetEmojiFromUnicode(unicode) {
    return String.fromCodePoint(parseInt(unicode, 16));
    // return unicode;
  }

  getCountriesArray() {
    const json = require("./countries.json");
    var arr = json.map(row => ({ value: row.code, label: row.name }));
    return arr;
  }

  ParseResponse() {
    var data = this.state.data;
    data.map(x => (x.emoji = this.GetEmojiFromUnicode(x.emoji)));
    this.setState({ data: data, isLoaded: true });
  }

  render() {
    return (
      <React.Fragment>
        <br />
        <br />
        <b>Pick date:</b>&nbsp;&nbsp;
        <DatePicker
          selected={this.state.startDate}
          onChange={this.handleDateChange.bind(this)}
        />
        &nbsp;&nbsp;&nbsp;
        <Button
          className="ui right clearing segment"
          onClick={this.getData}
          color="teal"
          size="big"
          disabled={!this.state.isCountrySelected || !this.state.isDateSelected}
        >
          Load data
        </Button>
        <br />
        <b>Pick country:</b>&nbsp;&nbsp;
        <Select
          placeholder="Select country"
          styles={{ width: "50%" }}
          name="countries"
          options={this.getCountriesArray()}
          className="basic-multi-select"
          classNamePrefix="select"
          onChange={this.handleCountryChange.bind(this)}
          theme={theme => ({
            ...theme,
            borderRadius: 0,
            colors: {
              ...theme.colors,
              primary25: "primary",
              primary: "primary"
            }
          })}
        />
        {this.state.isLoading ? (
          <div style={{ marginTop: 50 }}>
            <center>
              <Loader active inline="centered">
                Loading...
              </Loader>
            </center>
          </div>
        ) : null}
        {this.state.isLoaded && this.state.data.length > 0 ? (
          <center>
            <br />
            <table id="emoji">
              <tbody>
                <tr key="header">
                  <th>Emoji</th>
                  <th>Value</th>
                </tr>
                {this.state.data.map(dynamicData => {
                  return (
                    <tr key={dynamicData.emoji}>
                      <td> {dynamicData.emoji}</td>
                      <td> {dynamicData.value} </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </center>
        ) : this.state.isLoaded && this.state.data.length === 0 ? (
          <div style={{ marginTop: 20 }}>
            <center>No data avilable</center>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

ReactDOM.render(<TopPerDateAndCountry />, document.getElementById("root"));

export default TopPerDateAndCountry;
