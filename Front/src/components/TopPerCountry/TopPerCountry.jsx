import React, { Component } from "react";
import Select from "react-select";
import { Loader, Button } from "semantic-ui-react";

import "./style.css";

class TopPerCountry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      tableData: [],
      isLoaded: false,
      isLoading: false,
      finaloading: false,
      selectedCountries: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.getData = this.getData.bind(this);
  }
  getData() {
    this.setState({ isLoaded: false, isLoading: true });
    fetch("http://emojilizer.tk:8080/api/top_per_country", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        countries: this.state.selectedCountries
      })
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.ParseResponse(json);
        this.setState({ data: json });
      });
  }

  handleChange(SelectedCountries) {
    var arr = SelectedCountries.map(x => x.value);
    this.setState({ isLoaded: false, selectedCountries: arr });
  }

  GetEmojiFromUnicode(unicode) {
    return String.fromCodePoint(parseInt(unicode, 16));
  }

  ParseResponse(data) {
    this.state.selectedCountries.forEach(country => {
      if (!(country in data)) {
        return null;
      }
    });
    for (let i = 0; i < this.state.selectedCountries.length; i++) {
      data[this.state.selectedCountries[i]].map(
        x => (x.emoji = this.GetEmojiFromUnicode(x.emoji))
      );
    }

    this.setState({
      tableData: data,
      isLoaded: true,
      isLoading: false
    });
  }

  getCountriesArray() {
    const json = require("./countries.json");
    var arr = json.map(row => ({ value: row.code, label: row.name }));
    return arr;
  }

  getTables() {
    var tableData = this.state.tableData;
    var selectedCountries = this.state.selectedCountries;

    selectedCountries.forEach(country => {
      if (!(country in tableData)) {
        return null;
      }
    });
    return (
      <div id="tables" className="tablesContainer">
        {selectedCountries.map(country => {
          return (
            <center>
              <div className="countryTableContainer">
                {country}
                <table id="emoji">
                  <tbody>
                    <tr key="header">
                      <th>Emoji</th>
                      <th>Value</th>
                    </tr>
                    {tableData[country].map(dynamicData => {
                      return (
                        <tr key={dynamicData.emoji}>
                          <td> {dynamicData.emoji}</td>
                          <td> {dynamicData.value} </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </center>
          );
        })}
      </div>
    );
  }

  render() {
    return (
      <React.Fragment>
        <Button
          className="ui right clearing segment"
          onClick={this.getData}
          color="teal"
          size="big"
          disabled={
            this.state.isLoaded ||
            this.state.selectedCountries.length === 0 ||
            this.state.selectedCountries.length > 10
          }
        >
          Load data
        </Button>
        <div>
          <Select
            placeholder="Select countries (note: max 10 countries)"
            styles={{ width: "50%" }}
            isMulti
            name="countries"
            options={this.getCountriesArray()}
            className="basic-multi-select"
            classNamePrefix="select"
            onChange={newValue => this.handleChange(newValue)}
            theme={theme => ({
              ...theme,
              borderRadius: 0,
              colors: {
                ...theme.colors,
                primary25: "primary",
                primary: "primary"
              }
            })}
          />
          <br />
          {this.state.isLoading ? (
            <div style={{ marginTop: 20 }}>
              <center>
                <Loader active inline="centered">
                  Loading...
                </Loader>
              </center>
            </div>
          ) : null}

          {this.state.isLoaded && Object.keys(this.state.data).length > 0 ? (
            this.getTables()
          ) : this.state.isLoaded &&
            Object.keys(this.state.data).length === 0 ? (
            <div style={{ marginTop: 20 }}>
              <center>No data avilable</center>
            </div>
          ) : null}
        </div>
      </React.Fragment>
    );
  }
}

export default TopPerCountry;
