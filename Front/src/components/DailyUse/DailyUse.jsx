import React, { Component } from "react";
import FusionCharts from "fusioncharts";
import TimeSeries from "fusioncharts/fusioncharts.timeseries";
import ReactFC from "react-fusioncharts";
import "emoji-mart/css/emoji-mart.css";
import { Button } from "semantic-ui-react";

ReactFC.fcRoot(FusionCharts, TimeSeries);

class DailyUse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postQuery: [],
      isLoaded: false,
      timeseriesDs: {
        type: "timeseries",
        renderAt: "container",
        width: "100%",
        height: 450,
        dataSource: {
          chart: {
            showLegend: 0
          },
          caption: {
            text: "The amount of use of the emojis in the selected period"
          },
          yAxis: [
            {
              plot: {
                value: "Amount",
                type: "area"
              },
              title: "Emoji amount (in thousand)"
            }
          ],
          data: null
        }
      }
    };
    this.handleClick = this.handleClick.bind(this);
    this.createDataTable = this.createDataTable.bind(this);
  }

  handleClick(event) {
    switch (event) {
      case 1:
        this.setState({ postQuery: "last_7_days" }, () =>
          this.createDataTable()
        );
        break;
      case 2:
        this.setState({ postQuery: "last_14_days" }, () =>
          this.createDataTable()
        );
        break;
      case 3:
        this.setState({ postQuery: "last_30_days" }, () =>
          this.createDataTable()
        );
        break;
      case 4:
        this.setState({ postQuery: "last_60_days" }, () =>
          this.createDataTable()
        );
        break;
      default:
        console.log("at default");
    }
  }

  createDataTable() {
    this.setState({ isLoaded: false });
    const jsonify = res => res.json();
    const dataFetch = fetch("http://emojilizer.tk:8080/api/daily_use", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        range: this.state.postQuery
      })
    }).then(jsonify);

    Promise.all([dataFetch]).then(res => {
      const data = res[0];
      const schema = [
        {
          name: "Time",
          type: "date",
          format: "%Y-%m-%d"
        },
        {
          name: "Emoji amount",
          type: "number"
        }
      ];
      const fusionDataStore = new FusionCharts.DataStore();
      const fusionTable = fusionDataStore.createDataTable(data, schema);
      const timeseriesDs = Object.assign({}, this.state.timeseriesDs);
      timeseriesDs.dataSource.data = fusionTable;
      this.setState({
        timeseriesDs,
        isLoaded: true
      });
    });
  }

  render() {
    return (
      <span className="App">
        <span style={{ marginTop: 5, marginBottom: 5 }}>
          <Button onClick={() => this.handleClick(1)} color="teal" size="big">
            Last 7 days
          </Button>
          &nbsp;&nbsp;
          <Button onClick={() => this.handleClick(2)} color="teal" size="big">
            Last 14 days
          </Button>
          &nbsp;&nbsp;
          <Button onClick={() => this.handleClick(3)} color="teal" size="big">
            Last 30 days
          </Button>
          &nbsp;&nbsp;
          <Button onClick={() => this.handleClick(4)} color="teal" size="big">
            Last 60 days
          </Button>
        </span>
        {this.state.isLoaded ? <ReactFC {...this.state.timeseriesDs} /> : null}
      </span>
    );
  }
}

export default DailyUse;
