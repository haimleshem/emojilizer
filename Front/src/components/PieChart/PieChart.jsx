import React, { Component } from "react";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

class PieChart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: null,
      isLoaded: false
    };
  }

  componentDidMount() {
    fetch("http://emojilizer.tk:8080/api/top_ten_total", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" }
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.setState({
          items: json.result,
          isLoaded: true
        });
      });
  }

  setChart() {
    var myDataSource = {
      chart: {
        caption: "The distribution of use of Emoji by country",
        subCaption: "",
        xAxisName: "Country",
        yAxisName: "Number of Emoji",
        numberSuffix: "K",
        theme: "fusion"
      },
      data: this.state.items
    };

    var chartConfigs = {
      type: "Pie3D",
      width: "100%",
      height: 450,
      dataFormat: "json",
      dataSource: myDataSource
    };

    return <ReactFC {...chartConfigs} />;
  }

  render() {
    var isLoaded = this.state.isLoaded;
    return <React.Fragment>{isLoaded ? this.setChart() : null}</React.Fragment>;
  }
}

// ReactDOM.render(<PieChart />, document.getElementById("root"));

export default PieChart;
