import React, { Component } from "react";
import ReactDOM from "react-dom";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import { Loader, Button, Modal } from "semantic-ui-react";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

const myDataSource = {
  chart: {
    caption: "Global emoji distribution by top ten countries",
    subCaption: "",
    xAxisName: "Country",
    yAxisName: "Number of Emoji",
    numberSuffix: "K",
    theme: "fusion"
  },
  data: []
};

const chartConfigs = {
  type: "column2d",
  top: "10%",
  width: "100%",
  height: 450,
  dataFormat: "json",
  dataSource: myDataSource
};

class Distribution extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      showEmojiPicker: false,
      isLoading: false,
      isLoaded: false,
      selectedEmoji: null
    };

    this.handleClick = this.handleClick.bind(this);
    this.handleEmojiSelected = this.handleEmojiSelected.bind(this);
  }

  handleClick() {
    this.setState({ showEmojiPicker: true });
  }

  handleEmojiSelected(emoji) {
    this.setState({
      isLoading: true,
      showEmojiPicker: false,
      isLoaded: false,
      selectedEmoji: emoji.native
    });
    fetch("http://emojilizer.tk:8080/api/emoji_per_country", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        emoji: emoji.unified
      })
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        myDataSource.data = json;
        this.setState({
          data: json,
          isLoaded: true,
          isLoading: false
        });
      });
  }

  printEmojiPicker() {
    return (
      <center>
        <Picker
          onSelect={this.handleEmojiSelected}
          title="emoji color:"
          emoji="point_up"
          // skin="2"
          set="apple"
          emojiSize={32}
          // sheetSize={64}
        />
      </center>
    );
  }

  render() {
    const inlineStyle = {
      modal: {
        marginTop: "0px !important",
        top: "0px",
        display: "flex !important"
      }
    };
    var showPicker = this.state.showEmojiPicker;
    return (
      <span style={{ marginTop: 5, marginBottom: 5 }} className="rowC">
        <Modal
          trigger={
            <Button onClick={this.handleClick} color="teal" size="huge">
              Pick Emoji
            </Button>
          }
          centered={false}
          open={showPicker}
          style={inlineStyle.modal}
        >
          <Modal.Header>Please Pick an Emoji:</Modal.Header>
          <Modal.Content>
            <Modal.Description>{this.printEmojiPicker()}</Modal.Description>
          </Modal.Content>
          <Button
            floated="right"
            className="ui right clearing segment"
            onClick={this.handleEmojiSelected}
            primary
            size="huge"
          >
            Close
          </Button>
        </Modal>
        <center>
          {this.state.isLoading ? (
            <div>
              <br /> <br />
              <br />
              <Loader active inline="centered">
                Loading...
              </Loader>
            </div>
          ) : null}
          {this.state.isLoaded ? (
            <div>
              <b>Selected Emoji:</b> {this.state.selectedEmoji}
              <ReactFC {...chartConfigs} />{" "}
            </div>
          ) : null}
        </center>
      </span>
    );
  }
}

ReactDOM.render(<Distribution />, document.getElementById("root"));

export default Distribution;
