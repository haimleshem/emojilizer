import React, { Component } from "react";
import ReactDOM from "react-dom";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import "emoji-mart/css/emoji-mart.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Loader } from "semantic-ui-react";

import "./style.css";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

class TopPerDate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: new Date(),
      isLoaded: false,
      isLoading: false,
      data: []
    };
  }

  handleChange(date) {
    this.setState({
      startDate: date,
      isLoading: true,
      isLoaded: false
    });
    fetch("http://emojilizer.tk:8080/api/search_by_date", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        date: date.toISOString()
      })
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.setState(
          {
            data: json.top,
            isLoaded: true,
            isLoading: false
          },
          () => this.ParseResponse()
        );
      });
  }

  GetEmojiFromUnicode(unicode) {
    return String.fromCodePoint(parseInt(unicode, 16));
  }

  ParseResponse() {
    var data = this.state.data;
    data.map(x => (x.emoji = this.GetEmojiFromUnicode(x.emoji)));
    this.setState({ data: data, isLoaded: true });
  }

  render() {
    return (
      <React.Fragment>
        <b>Pick date:</b>&nbsp;&nbsp;
        <DatePicker
          selected={this.state.startDate}
          onChange={this.handleChange.bind(this)}
        />
        {this.state.isLoading ? (
          <center>
            <div>
              <br /> <br />
              <br />
              <Loader active inline="centered">
                Loading...
              </Loader>
            </div>
          </center>
        ) : null}
        {this.state.isLoaded && this.state.data.length > 0 ? (
          <center>
            <br />
            <table id="emoji">
              <tbody>
                <tr key="header">
                  <th>Emoji</th>
                  <th>Value</th>
                </tr>
                {this.state.data.map(dynamicData => {
                  return (
                    <tr key={dynamicData.emoji}>
                      <td> {dynamicData.emoji}</td>
                      <td> {dynamicData.value} </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </center>
        ) : this.state.isLoaded && this.state.data.length === 0 ? (
          <div style={{ marginTop: 20 }}>
            <center>No data avilable for this date</center>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

ReactDOM.render(<TopPerDate />, document.getElementById("root"));

export default TopPerDate;
