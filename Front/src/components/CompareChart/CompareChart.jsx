import React, { Component } from "react";
import FusionCharts from "fusioncharts";
import PowerCharts from "fusioncharts/fusioncharts.powercharts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";
import "emoji-mart/css/emoji-mart.css";

ReactFC.fcRoot(FusionCharts, PowerCharts, FusionTheme);

const chartConfigs = {
  type: "dragcolumn2d",
  width: "100%",
  height: 450,
  dataFormat: "json",
  dataSource: []
};

class CompareChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: ""
      // "Drag any column for years 2017 or 2018 to see updated value along with the label"
    };

    this.dataplotDragEnd = this.dataplotDragEnd.bind(this);
  }

  // Event callback handler for 'dataplotDragEnd' event.
  // Shows a message with the dateset, initial value and final value of the dragged column.
  dataplotDragEnd(eventObj, dataObj) {
    var prevValue = FusionCharts.formatNumber(dataObj.startValue.toFixed(2));
    var curValue = FusionCharts.formatNumber(dataObj.endValue.toFixed(2));
    var labelYear = this.state.chart.args.dataSource.categories[0].category[
      dataObj.dataIndex
    ].label;
    this.setState({
      message: [
        <strong>{eventObj.data.datasetName}</strong>,
        " is modified to ",
        <strong>{"K" + curValue + "M"}</strong>,
        " from ",
        <strong>{"K" + prevValue + "M"}</strong>,
        " for ",
        <strong>{labelYear}</strong>
      ]
    });
  }

  componentDidMount() {
    fetch("http://emojilizer.tk:8080/api/compare_chart", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" }
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        chartConfigs.dataSource = json;
        this.setState({
          data: json,
          isLoaded: true
        });
      });
  }

  render() {
    return (
      <div>
        <ReactFC
          {...chartConfigs}
          fcEvent-dataplotDragEnd={this.dataplotDragEnd}
        />
        <p style={{ padding: "10px", background: "#f5f2f0" }}>
          {this.state.message}
        </p>
      </div>
    );
  }
}

export default CompareChart;
