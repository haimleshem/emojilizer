import React, { Component } from "react";
import FusionCharts from "fusioncharts";
import Charts from "fusioncharts/fusioncharts.charts";
import ReactFC from "react-fusioncharts";
import FusionTheme from "fusioncharts/themes/fusioncharts.theme.fusion";

ReactFC.fcRoot(FusionCharts, Charts, FusionTheme);

class WeekCompare extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: null,
      isLoaded: false
    };
  }
  componentDidMount() {
    fetch("http://emojilizer.tk:8080/api/four_weeks", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" }
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.setState({
          items: json.result,
          isLoaded: true
        });
      });
  }

  setChart() {
    var myDataSource = {
      chart: {
        caption: "Collected data in the last four weeks",
        subCaption: null,
        xAxisname: "Weeks",
        yAxisName: "Amount",
        numberPrefix: "",
        theme: "fusion"
      },
      categories: [
        {
          category: [
            {
              label: "Week 1"
            },
            {
              label: "Week 2"
            },
            {
              label: "Week 3"
            },
            {
              label: "Week 4"
            }
          ]
        }
      ],
      dataset: this.state.items
    };

    var chartConfigs = {
      type: "mscombi2d",
      width: "100%",
      height: 450,
      dataFormat: "json",
      dataSource: myDataSource
    };
    return <ReactFC {...chartConfigs} />;
  }

  render() {
    var isLoaded = this.state.isLoaded;
    return (
      <div>
        <center>
          <React.Fragment>{isLoaded ? this.setChart() : null}</React.Fragment>
        </center>
      </div>
    );
  }
}

export default WeekCompare;
