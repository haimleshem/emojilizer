import React from "react";
import ReactDOM from "react-dom";
import { Message, Checkbox, Button } from "semantic-ui-react";

import "./style.css";

class Download extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isAccepted: false };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    if (this.state.isAccepted === false) {
      this.setState({ isAccepted: true });
    } else {
      this.setState({ isAccepted: false });
    }
  }

  render() {
    return (
      <div style={{ marginTop: 30 }}>
        <Message>
          <center>
            <Message.Header>Terms of Use</Message.Header>
          </center>
          <p>
            The site and the information inside it was built as part of an
            academic final project under the Sami Shamoon College.
            <br />
            The information may not be used without the written permission of
            the authors.
          </p>
        </Message>
        &nbsp;&nbsp;
        <Checkbox
          label="I agree to the Terms and Conditions"
          onChange={this.handleChange}
        />
        <br />
        <center>
          <div className="download">
            <form method="get" action="http://emojilizer.tk:8080/download/geo">
              <Button
                className="ui right clearing segment"
                onClick={this.getData}
                color="teal"
                size="huge"
                disabled={!this.state.isAccepted}
              >
                Download geo-information
              </Button>
            </form>
            &nbsp;&nbsp;
            <form
              method="get"
              action="http://emojilizer.tk:8080/download/emoji"
            >
              <Button
                className="ui right clearing segment"
                onClick={this.getData}
                color="teal"
                size="huge"
                disabled={!this.state.isAccepted}
              >
                Download tweets with emoji
              </Button>
            </form>
            &nbsp;&nbsp;
            <form
              method="get"
              action="http://emojilizer.tk:8080/download/non-emoji"
            >
              <Button
                className="ui right clearing segment"
                onClick={this.getData}
                color="teal"
                size="huge"
                disabled={!this.state.isAccepted}
              >
                Download tweets without emoji
              </Button>
            </form>
          </div>
        </center>
      </div>
    );
  }
}

ReactDOM.render(<Download />, document.getElementById("root"));
export default Download;
