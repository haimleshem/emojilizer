import React from "react";
import ReactDOM from "react-dom";
import { Image } from "semantic-ui-react";
import "./style.css";

class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <center>
          <br />
          <Image
            src="https://en.sce.ac.il/ver/5/tpl/website/img/en_logo11.png"
            size="small"
          />
          <div style={{ marginBottom: 390 }}>
            <br />
            <h3>
              <b>This project was made by Maor Israel and Haim Leshem</b>
            </h3>
            <a href="https://gitlab.com/haimleshem/emojilizer">
              <h4>
                <b> Check our GitLab</b>
              </h4>
            </a>
          </div>
          <div className="bottomImage" />
          <Image
            src="https://en.sce.ac.il/filestock/file/1554977168444-0.jpg"
            width="100%"
            height="35%"
          />
        </center>
      </div>
    );
  }
}

ReactDOM.render(<About />, document.getElementById("root"));
export default About;
