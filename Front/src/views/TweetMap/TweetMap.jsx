import React from "react";
import ReactDOM from "react-dom";
import mapboxgl from "mapbox-gl";
import { Picker } from "emoji-mart";
import { Loader, Button, Modal } from "semantic-ui-react";

import "mapbox-gl/dist/mapbox-gl.css";

mapboxgl.accessToken =
  "pk.eyJ1IjoibWFvcjg0IiwiYSI6ImNqcTVrejI1bjBrMXo0NGxzNHY0N285MzEifQ.inLRmUNoRABCiaRmJQTvHA";

class TweetMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      geoJsonData: [],
      isLoaded: false,
      isLoading: false,
      lng: 5,
      lat: 34,
      zoom: 1.5,
      showEmojiPicker: false,
      selectedEmoji: null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleEmojiSelected = this.handleEmojiSelected.bind(this);
  }

  handleClick() {
    this.setState({ showEmojiPicker: true });
  }

  handleEmojiSelected(emoji) {
    this.setState({
      showEmojiPicker: false,
      isLoaded: false,
      isLoading: true,
      selectedEmoji: emoji.native
    });
    fetch("http://emojilizer.tk:8080/api/geojson", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        emoji: emoji.unified
      })
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.setState(
          {
            isLoaded: true,
            geoJsonData: json
          },
          () => this.componentDidMount()
        );
      });
  }

  printEmojiPicker() {
    return (
      <center>
        <Picker
          onSelect={this.handleEmojiSelected}
          title="emoji color:"
          emoji="point_up"
          // skin="2"
          set="apple"
          emojiSize={32}
          // sheetSize={64}
        />
      </center>
    );
  }

  componentDidMount() {
    const { lng, lat, zoom } = this.state;

    var map = new mapboxgl.Map({
      container: this.mapContainer,
      style: "mapbox://styles/mapbox/streets-v11",
      center: [lng, lat],
      zoom
    });
    if (this.state.isLoaded) {
      var geoJsonData = this.state.geoJsonData;
      map.on("load", function() {
        map.addSource("tweetPoints", {
          type: "geojson",
          data: geoJsonData,
          cluster: true,
          clusterMaxZoom: 14, // Max zoom to cluster points on
          clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
        });
        map.addLayer({
          id: "clusters",
          type: "circle",
          source: "tweetPoints",
          filter: ["has", "point_count"],
          paint: {
            "circle-color": [
              "step",
              ["get", "point_count"],
              "#ffea96",
              100,
              "#fea843",
              750,
              "#fc4426"
            ],
            "circle-radius": [
              "step",
              ["get", "point_count"],
              20,
              100,
              30,
              750,
              40
            ]
          }
        });

        map.addLayer({
          id: "cluster-count",
          type: "symbol",
          source: "tweetPoints",
          filter: ["has", "point_count"],
          layout: {
            "text-field": "{point_count_abbreviated}",
            "text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
            "text-size": 12
          }
        });

        map.addLayer({
          id: "unclustered-point",
          type: "circle",
          source: "tweetPoints",
          filter: ["!", ["has", "point_count"]],
          paint: {
            "circle-color": "#11b4da",
            "circle-radius": 4,
            "circle-stroke-width": 1,
            "circle-stroke-color": "#fff"
          }
        });

        // inspect a cluster on click
        map.on("click", "clusters", function(e) {
          var features = map.queryRenderedFeatures(e.point, {
            layers: ["clusters"]
          });
          var clusterId = features[0].properties.cluster_id;
          map
            .getSource("tweetPoints")
            .getClusterExpansionZoom(clusterId, function(err, zoom) {
              if (err) return;

              map.easeTo({
                center: features[0].geometry.coordinates,
                zoom: zoom
              });
            });
        });

        map.on("mouseenter", "clusters", function() {
          map.getCanvas().style.cursor = "pointer";
        });
        map.on("mouseleave", "clusters", function() {
          map.getCanvas().style.cursor = "";
        });

        // map.on("move", () => {
        //   const { lng, lat } = map.getCenter();
        //   console.log(lng + lat);
        //   this.setState({
        //     lng: lng.toFixed(4),
        //     lat: lat.toFixed(4),
        //     zoom: map.getZoom().toFixed(2)
        //   });
        // });
      });
      this.setState({ isLoaded: true, isLoading: false });
    }

    map.on("mouseenter", "unclustered-point", function() {
      map.getCanvas().style.cursor = "pointer";
    });

    // Change it back to a pointer when it leaves.
    map.on("mouseleave", "unclustered-point", function() {
      map.getCanvas().style.cursor = "";
    });

    map.on("click", "unclustered-point", function(e) {
      var coordinates = e.features[0].geometry.coordinates.slice();
      // var description = e.features[0].properties.description;
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
        coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
      }

      // new mapboxgl.Popup()
      //   .setLngLat(coordinates)
      //   .setHTML(description)
      //   .addTo(map);
    });
  }

  render() {
    const { lng, lat, zoom } = this.state;
    var rootStyle = {
      height: "84%",
      minHeight: "84%"
    };
    const inlineStyle = {
      modal: {
        marginTop: "0px !important",
        top: "0px",
        display: "flex !important"
      }
    };
    var showPicker = this.state.showEmojiPicker;

    return (
      <React.Fragment>
        <Modal
          trigger={
            <div style={{ margin: "15px" }}>
              <Button
                onClick={this.handleClick}
                color="teal"
                size="huge"
                floated="left"
              >
                Pick Emoji
              </Button>
            </div>
          }
          centered={false}
          open={showPicker}
          style={inlineStyle.modal}
        >
          <Modal.Header>Please Pick an Emoji:</Modal.Header>
          <Modal.Content>
            <Modal.Description>{this.printEmojiPicker()}</Modal.Description>
          </Modal.Content>
          <Button
            floated="right"
            className="ui right clearing segment"
            onClick={this.handleEmojiSelected}
            primary
            size="huge"
          >
            Close
          </Button>
        </Modal>
        <br />
        {this.state.isLoading ? (
          <span>
            <center>
              <Loader active inline="centered">
                Loading...
              </Loader>
            </center>
          </span>
        ) : null}
        {this.state.isLoaded ? (
          <span>
            <center>
              <b>Selected Emoji: {this.state.selectedEmoji}</b>
            </center>
          </span>
        ) : null}
        <div className="content">
          <div className="inline-block absolute bottom left bg-darken75 color-white z1 py6 px12 round-full txt-s txt-bold">
            <div>{`Longitude: ${lng} Latitude: ${lat} Zoom: ${zoom}`}</div>
          </div>
          <div style={rootStyle} ref={el => (this.mapContainer = el)} />
        </div>
      </React.Fragment>
    );
  }
}

export default TweetMap;
ReactDOM.render(<TweetMap />, document.getElementById("root"));
