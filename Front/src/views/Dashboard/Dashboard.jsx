import React, { Component } from "react";
import { Grid, Row, Col } from "react-bootstrap";
import { StatsCard } from "components/StatsCard/StatsCard.jsx";
import PieChart from "components/PieChart/PieChart.jsx";
import WeekCompare from "components/WeekCompare/WeekCompare.jsx";
import Emoji from "a11y-react-emoji";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      isLoaded: false
    };
  }

  componentDidMount() {
    fetch("http://emojilizer.tk:8080/api/dashboard", {
      crossDomain: true,
      method: "POST",
      headers: { "Content-Type": "application/json" }
    })
      .then(
        res => res.json(),
        error => console.log("An error occurred.", error)
      )
      .then(json => {
        this.setState({
          items: json,
          items2: this.state.items2,
          isLoaded: true
        });
      });
  }

  unicodeToChar(text) {
    return text.replace(/\\u[\dA-F]{4}/gi, function(match) {
      return String.fromCharCode(parseInt(match.replace(/\\u/g, ""), 16));
    });
  }

  render() {
    var { items, isLoaded } = this.state;
    // const arr = items.map(item => item.address.zipcode);
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="fa fa-twitter text-info" />}
                statsText="Total Tweets"
                statsValue={isLoaded ? items.total_tweets : "Loading..."}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="fa fa-map-marker text-danger" />}
                statsText="Geo Tweets"
                statsValue={isLoaded ? items.total_geo : "Loading..."}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard // {<Emoji symbol="😂" />}
                bigIcon={<i className="fa fa-smile-o text-Dark" />}
                statsText="Most common"
                statsValue={
                  isLoaded ? (
                    <Emoji
                      symbol={String.fromCodePoint(
                        parseInt(items.most_common.substring(4), 16)
                      )}
                    />
                  ) : (
                    "Loading..."
                  )
                }
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
            <Col lg={3} sm={6}>
              <StatsCard
                bigIcon={<i className="fa fa-line-chart text-Primary" />}
                statsText="Text/Emoji Ratio"
                statsValue={isLoaded ? items.ratios.avg_emoji : "Loading..."}
                statsIcon={<i className="fa fa-refresh" />}
                statsIconText="Updated now"
              />
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <WeekCompare />
            </Col>
            <Col md={6}>
              <PieChart />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
