import React from "react";
import ReactDOM from "react-dom";
import EmojiDistribution from "components/EmojiDistribution/EmojiDistribution.jsx";
import DailyUse from "components/DailyUse/DailyUse.jsx";
import CompareChart from "components/CompareChart/CompareChart.jsx";
import TopPerDate from "components/TopPerDate/TopPerDate.jsx";
import WeeklyUse from "components/WeeklyUse/WeeklyUse.jsx";
import TopPerCountry from "components/TopPerCountry/TopPerCountry.jsx";
import TopPerDateAndCountry from "components/TopPerDateAndCountry/TopPerDateAndCountry.jsx";
import "emoji-mart/css/emoji-mart.css";
import { Dropdown } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";

const options = [
  { key: 1, text: "Distribution by emoji", value: 1 },
  { key: 2, text: "Emoji use by time", value: 2 },
  { key: 3, text: "Ratio: length and emojis", value: 3 },
  { key: 4, text: "Weekly use by emoji", value: 4 },
  { key: 5, text: "Top ten by date", value: 5 },
  { key: 6, text: "Top ten by country", value: 6 },
  { key: 7, text: "Top ten by date and country", value: 7 }
];

function showSelectedChart(chartType) {
  switch (chartType) {
    case 1:
      return <EmojiDistribution />;
    case 2:
      return <DailyUse />;
    case 3:
      return <CompareChart />;
    case 4:
      return <WeeklyUse />;
    case 5:
      return <TopPerDate />;
    case 6:
      return <TopPerCountry />;
    case 7:
      return <TopPerDateAndCountry />;
    default:
      return null;
  }
}

class Statistics extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedOption: 1,
      showEmojiPicker: false
    };

    this.handleDropdownChange = this.handleDropdownChange.bind(this);
  }

  handleDropdownChange(event, data) {
    const { value } = data;
    this.setState({ selectedOption: value });
  }

  render() {
    return (
      <React.Fragment>
        &nbsp;Statistics options:&nbsp;
        <Dropdown
          value={this.state.selectedOption}
          onChange={this.handleDropdownChange}
          clearable
          selection
          options={options}
        />
        &nbsp;
        {showSelectedChart(this.state.selectedOption)}
      </React.Fragment>
    );
  }
}

ReactDOM.render(<Statistics />, document.getElementById("root"));
export default Statistics;
