import re
import time

from tweepy.streaming import StreamListener, json
from tweepy import OAuthHandler
from tweepy import Stream
import twitter_credentials
from Back.Utilities import producer


class TwitterStreamer():

    def __init__(self):
        self.twitterAuthenticator = TwitterAuthenticator()

    def stream_tweets(self):
        # Handle authentication and connect to the streaming API
        listener = TwitterListener()
        auth = self.twitterAuthenticator.authenticate()
        stream = Stream(auth, listener)
        stream.sample()


class TwitterAuthenticator:
    def authenticate(self):
        auth = OAuthHandler(twitter_credentials.CONSUMER_KEY, twitter_credentials.CONSUMER_SECRET)
        auth.set_access_token(twitter_credentials.ACCESS_TOKEN, twitter_credentials.ACCESS_TOKEN_SECRET)
        return auth


class TwitterListener(StreamListener):

    def __init__(self, ):
        super().__init__()
        self.jsonDict = dict()
        self.emoji_reg = re.compile(r'(U0001f([a-z]|[0-9])([a-z]|[0-9])([a-z]|[0-9]))')

    def on_data(self, data):
        try:
            producer.send_tweet(data)
            return

        except BaseException as e:
            if str(e) != "'place'" and str(e) != "'text'":
                print('Error on data: %s' % str(e))
        return True

    def on_error(self, status):
        if status == 420:
            print('Rate limit reached. Sleeping for 15 minutes')
            time.sleep(60 * 15)
            return False
        print(status)

    def on_exception(self, exception):
        print(exception)
        TwitterStreamer().stream_tweets()
        return
