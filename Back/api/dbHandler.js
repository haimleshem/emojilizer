// import {toUnicode} from "node/punycode";

var MongoClient = require('mongodb').MongoClient;
const dbName = "Tweets";
const url = "mongodb://localhost:27017";
const fs = require('fs');
const punycode = require('punycode');

require("datejs");


// Connect to the db
const connection = () => new Promise((resolve, reject) => {
    var client = new MongoClient(url, {useNewUrlParser: true});
    client.connect((err, db) => {
        if (err) reject(err);
        else {
            console.log("connected to DB");
            resolve(client.db(dbName))
        }
    })
});

exports.dashboard = () => new Promise(resolve => {
    let rawdata = fs.readFileSync('scripts/dashboard.json');
    let parsed = JSON.parse(rawdata);
    resolve(parsed)
});

exports.geojson = (emoji) => new Promise(resolve => {
    connection().then(db => {
        let arr = [];
        emoji = "U000" + emoji;
        db.collection('tweets_geo').find({
            'properties.emoji_list': {$all: [emoji]},
            'properties.created_at': {$gte: new Date(Date.today().add(-14).days())}
        }).forEach((item) => {
            arr.push({
                "type": "Feature",
                "geometry": {"type": "Point", "coordinates": item.geometry.coordinates},

            })
        }).then(() => {
            let res = {"type": "FeatureCollection", "features": arr};
            resolve(res)
        })
    })
});

exports.top_ten_total = () => new Promise(resolve => {
    let rawdata = fs.readFileSync('scripts/top_ten_total.json');
    let parsed = JSON.parse(rawdata);
    resolve(parsed)
});

exports.four_weeks = () => new Promise(resolve => {
    let rawdata = fs.readFileSync('scripts/four_weeks.json');
    let parsed = JSON.parse(rawdata);
    resolve(parsed)
});

exports.top_per_country = (country_codes) => new Promise(resolve => {
    connection().then(db => {
        let resp = {};
        let promises = country_codes.map(country => new Promise((resolve1, reject1) => {
            let emojis = {};
            db.collection('tweets_emoji').find({'place.country_code': country}, {emoji_list: 1}).forEach((item) => {
                item.emoji_list.forEach((emoji) => {
                    emoji = emoji.replace('U000', '');
                    if (emojis.hasOwnProperty(emoji) && ['1f3fb', '1f3fc', '1f3fd', '1f3fe', '1f3ff'].indexOf(emoji) === -1) {
                        emojis[emoji] += 1;
                    } else {
                        emojis[emoji] = 1
                    }
                })
            }).then(() => {
                let keys = Object.keys(emojis);
                let values = Object.values(emojis);
                values.sort((a, b) => {
                    return b - a
                });
                keys.sort((a, b) => {
                    return emojis[b] - emojis[a]
                });
                let sortedKeys = keys.slice(0, 10);
                let sortedValues = values.slice(0, 10);
                let arr = [];
                for (let i = 0; i < sortedValues.length; i++) {
                    arr.push({'emoji': sortedKeys[i], 'value': sortedValues[i]})
                }
                resp[country] = arr;
                resolve1(resp)

            })
        }));
        Promise.all(promises).then(results => {
            resolve(results[0])
        })
    })
});

exports.search_by_date = (date) => new Promise(resolve => {
    connection().then(db => {
        let emojis = {};

        db.collection("tweets_emoji").find({
                "created_at": {
                    $gte: new Date(new Date(date).setUTCHours(0, 0, 0)),
                    $lte: new Date(new Date(date).setUTCHours(23, 59, 59))
                }
            }, {emoji_list: 1}
        ).forEach((item) => {
            item.emoji_list.forEach((emoji) => {
                emoji = emoji.replace('U000', '');

                if (['1f3fb', '1f3fc', '1f3fd', '1f3fe', '1f3ff'].indexOf(emoji) === -1) {
                    if (emojis.hasOwnProperty(emoji)) {
                        emojis[emoji] += 1;
                    } else {
                        emojis[emoji] = 1
                    }
                }

            })
        }).then(() => {
            let keys = Object.keys(emojis);
            let values = Object.values(emojis);
            values.sort((a, b) => {
                return b - a
            });
            keys.sort((a, b) => {
                return emojis[b] - emojis[a]
            });
            let sortedKeys = keys.slice(0, 10);
            let sortedValues = values.slice(0, 10);
            let arr = [];
            for (let i = 0; i < sortedValues.length; i++) {
                arr.push({'emoji': punycode.toUnicode(sortedKeys[i]), 'value': sortedValues[i]})
            }
            resolve({"top": arr})
        })
    })
});

exports.search_date_location = (date, country) => new Promise(resolve => {
    connection().then(db => {
        let emojis = {};
        db.collection("tweets_geo").find({
                "properties.created_at": {
                    $gte: new Date(new Date(date).setUTCHours(0, 0, 0)),
                    $lte: new Date(new Date(date).setUTCHours(23, 59, 59))
                },
                'properties.country_code': country
            }, {emoji_list: 1}
        ).forEach((item, err) => {
            if (typeof item.properties.emoji_list === 'undefined') {
                resolve("No available information");
                return;
            }
            item.properties.emoji_list.forEach((emoji) => {
                emoji = emoji.replace('U000', '');
                if (emojis.hasOwnProperty(emoji) && ['1f3fb', '1f3fc', '1f3fd', '1f3fe', '1f3ff'].indexOf(emoji) === -1) {
                    emojis[emoji] += 1;
                } else {
                    emojis[emoji] = 1
                }
            })
        }).then(() => {
            let keys = Object.keys(emojis);
            let values = Object.values(emojis);
            values.sort((a, b) => {
                return b - a
            });
            keys.sort((a, b) => {
                return emojis[b] - emojis[a]
            });
            let sortedKeys = keys.slice(0, 10);
            let sortedValues = values.slice(0, 10);
            let arr = [];
            for (let i = 0; i < sortedValues.length; i++) {
                arr.push({'emoji': sortedKeys[i], 'value': sortedValues[i]})
            }
            resolve({"top": arr})
        })
    })
});

exports.daily_use = (range) => new Promise(resolve => {
    connection().then(db => {
        console.log(range);
        switch (range) {
            case "last_24_hours":
                range = 1;
                // Date.today().add(-24).hours();
                break;
            case "last_7_days":
                range = 7;
                // Date.today().add(-7).days();
                break;
            case "last_14_days":
                range = 14;
                // Date.today().add(-14).days();
                break;
            case "last_30_days":
                range = 30;
                // Date.today().add(-30).days();
                break;
            case "last_60_days":
                range = 60;
                break;
        }

        let rawdata = fs.readFileSync('scripts/date_range.json');
        let arr = [];
        rawdata = JSON.parse(rawdata);
        if (rawdata.length < range) {
            resolve(rawdata);
        } else {
            rawdata = rawdata.slice(0, range);
            resolve(rawdata);
        }


    })
});

// exports.length_amount = (len) => new Promise(resolve => {
//     connection().then(db => {
//         console.log((parseInt(len, 10) + 10).toString());
//         let pipeline = [
//             {$match: {text_length: {$lte: len}}},
//             {$group: {_id: null, value: {$sum: "$emoji_count"}}}
//         ];
//         db.collection("tweets_emoji").aggregate(pipeline, {allowDiskUse: true}).forEach(item => {
//             console.log(item)
//         })
//
//
//     })
// });

exports.weekly_use = (emoji) => new Promise(resolve => {
    connection().then(db => {
        emoji = "U000" + emoji;
        let res = [];
        let days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        for (let i = -1; i >= -7; i--) {
            let date = new Date(Date.today().add(i).days());
            console.log(date.getDay())
            res.push({"label": days[date.getDay()], "value": 0});

            db.collection("tweets_emoji").find({
                "created_at": {
                    $gte: new Date(new Date(date).setUTCHours(0, 0, 0)),
                    $lte: new Date(new Date(date).setUTCHours(23, 59, 59))
                }, emoji_list: {$in: [emoji]}
            }, {emoji_list: 1}).forEach(item => {
                res[-i - 1]["value"] += item.emoji_list.filter((x) => {
                    return x === emoji
                }).length
            }).then(() => {
                if (i === -7)
                    resolve(res);
            })

        }

    })
});

exports.emoji_per_country = (emoji) => new Promise(resolve => {
    emoji = "U000" + emoji;
    let rawdata = fs.readFileSync('scripts/emoji_per_country.json');
    let parsed = JSON.parse(rawdata);
    resolve(parsed[emoji])
});

exports.compare_chart = () => new Promise(resolve => {
    let rawdata = fs.readFileSync('scripts/compare_chart.json');
    let parsed = JSON.parse(rawdata);
    resolve(parsed)
})