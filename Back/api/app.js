var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require("body-parser");
var api = require("./api");
var download = require("./download");

var cors = require("cors");

var app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use('/api', api);
app.get('/download/geo', (req, res) =>{
    const file = 'C:\\data\\downloads\\Geo.csv';
    res.download(file);
});
app.get('/download/emoji', (req, res) =>{
    const file = 'C:\\data\\downloads\\emoji.csv';
    res.download(file);
});
app.get('/download/non-emoji', (req, res) =>{
    const file = 'C:\\data\\downloads\\noemoji.csv';
    res.download(file);
});

app.listen(8080, () => {
    console.log("API Listening on port 8080")
});
