var express = require('express');
var app = express();
var router = express.Router();
var db = require("./dbHandler");

router.post("/dashboard", dashboard);
router.post("/geojson", geojson);
router.post("/four_weeks", four_weeks);
router.post("/top_ten_total", top_ten_total);
router.post("/top_per_country", top_per_country);
router.post("/search_by_date", search_by_date);
router.post("/search_date_location", search_date_location);
router.post("/daily_use", daily_use);
// router.post("/length_amount", length_amount);
router.post("/weekly_use",weekly_use);
router.post("/emoji_per_country", emoji_per_country);
router.post("/compare_chart", compare_chart);

module.exports = router;

async function dashboard(req, res, next) {
    let action = await db.dashboard();
    res.send(action)
}

async function geojson(req, res, next) {
    console.log(req.body.emoji);
    let action = await db.geojson(req.body.emoji);
    res.send(action)
}

async function four_weeks(req, res, next) {
    let action = await db.four_weeks();
    res.send(action)
}

async function top_ten_total(req, res, next) {
    let action = await db.top_ten_total();
    res.send(action)
}

async function top_per_country(req, res, next) {
    let action = await db.top_per_country(req.body.countries);
    res.send(action)
}

async function search_by_date(req, res, next) {
    console.log(req.body.date);
    let action = await db.search_by_date(req.body.date);
    res.send(action)
}

async function search_date_location(req, res, next) {
    let action = await db.search_date_location(req.body.date, req.body.country);
    res.send(action)
}

async function daily_use(req, res, next) {
    let action = await db.daily_use(req.body.range);
    res.send(action)
}

// async function length_amount(req, res, next) {
//     let action = await db.length_amount(req.body.length);
//     res.send(action)
// }

async function weekly_use(req, res, next) {
    console.log(req.body.emoji);
    let action = await db.weekly_use(req.body.emoji);
    res.send(action)
}

async function emoji_per_country(req, res, next) {
    let action = await db.emoji_per_country(req.body.emoji);
    res.send(action)
}

async function compare_chart(req, res, next) {
    let action = await db.compare_chart();
    res.send(action)
}