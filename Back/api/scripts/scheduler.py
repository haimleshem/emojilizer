from apscheduler.schedulers.blocking import BlockingScheduler
from Back.api.scripts import dashboard, emoji_per_country, four_weeks, top_ten_total, date_range, compare_chart,export

scheduler = BlockingScheduler()
scheduler.add_job(emoji_per_country.execute, 'interval', hours=2)
scheduler.add_job(four_weeks.four_weeks, 'interval', minutes=10)
scheduler.add_job(top_ten_total.top_ten_total, 'interval', minutes=10)
scheduler.add_job(date_range.date_range, 'interval', minutes=30)
scheduler.add_job(dashboard.build_dashboard, 'interval', minutes=10)
scheduler.add_job(compare_chart.compare_chart, 'interval', hours=24)
scheduler.add_job(export, 'interval', hours=24)


scheduler.start()
