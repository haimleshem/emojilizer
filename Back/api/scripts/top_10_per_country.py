import json
import operator

import pymongo
import time

from Back.Utilities import db_handler as dbfile


def top_10_per_country(country_codes):
    if len(country_codes) > 10:
        return {'error': 'value larger than 10'}
    if len(country_codes) == 0:
        return {'error': 'value cannot be 0'}
    resp = {}
    for country in country_codes:
        print(country)
        emojis = {}
        for item in dbfile.emoji_collection.find({'place.country_code': country}, {'emoji_list': 1}):
            for emoji in item['emoji_list']:
                if emoji in emojis:
                    emojis[emoji] += 1
                else:
                    emojis[emoji] = 1
        top_ten = list(sorted(emojis.items(), key=operator.itemgetter(1), reverse=True)[:10])
        temp = []
        for x in top_ten:
            temp.append({'emoji': x[0], 'amount': x[1]})
        resp[country] = temp
    with open('top_10_per_country.json', 'w') as out:
        json.dump(resp, out)


top_10_per_country(['FR', 'DE', 'IL', 'RU', 'CO', 'BR', 'GB', 'MX', 'US', 'KW'])
