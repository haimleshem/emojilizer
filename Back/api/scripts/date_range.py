import json
from datetime import datetime, timedelta
from Back.Utilities import db_handler as db
from time import time as ti


def date_range():
    start_time = ti()
    print("date_range starting")
    arr = []
    x = 0
    for i in range(0, 60):
        time = (datetime.now() - timedelta(days=i))
        pipeline = [
            {"$match": {"created_at": {"$gt": time}}},
            {"$group": {
                "_id": None,
                "value": {"$sum": "$emoji_count"},
            }
            }]
        result = db.new_emoji_collection.aggregate(pipeline, allowDiskUse=True)
        for item in result:
            date = "{0}-0{1}-{2}".format(time.year, time.month, time.day)
            x += 1
            arr.append([date, item['value']])
    with open("date_range.json", "w") as output:
        json.dump(arr, output)

    print("date_range finished after %s seconds" % (ti() - start_time))


date_range()