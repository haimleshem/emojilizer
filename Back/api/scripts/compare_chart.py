import json
import datetime
import time
import calendar
from Back.Utilities import db_handler as db
from dateutil.relativedelta import relativedelta
from dateutil import parser


def compare_chart():
    start_time = time.time()
    print("Compare chart started")
    data_emoji = []
    data_text = []
    category = []
    response = {
        "chart": {
            "caption": "Ratio between the tweet length and amount of emojis",
            "subCaption": "",
            "yaxismaxvalue": "140",
            "theme": "fusion",
            "plotToolText": "<b>$label</b><br>$seriesName: <b>$dataValue</b>"
        },
    }
    month = (datetime.datetime.now() + relativedelta(months=-1))
    two_months = datetime.datetime.now() + relativedelta(months=-2)
    three_months = datetime.datetime.now() + relativedelta(months=-3)
    four_months = datetime.datetime.now() + relativedelta(months=-4)
    months = [datetime.datetime.now(), month, two_months, three_months, four_months]
    for i in range(1, len(months)):

        category.insert(0, {"label": calendar.month_name[months[i].month], "fontItalic": "0"}),
        pipeline = [
            {"$match": {"created_at": {"$gte": parser.parse(months[i].isoformat()),
                                       "$lt": parser.parse(months[i - 1].isoformat())}}},
            {"$project": {"emoji_count": 1, "text_length": 1}},
            {"$group": {
                "_id": None,
                "emoji_count": {"$avg": "$emoji_count"},
                "text_length": {"$avg": "$text_length"}
            }
            }]
        result = db.new_emoji_collection.aggregate(pipeline, allowDiskUse=True)
        for item in result:
            data_emoji.insert(0, {"value": "%.2f" % item["emoji_count"], "alpha": "100", "allowDrag": "0"})
            data_text.insert(0, {"value": "%.2f" % item["text_length"], "alpha": "100", "allowDrag": "0"})
    response["categories"] = [{"category": category}]
    response["dataset"] = [{"seriesname": "Emoji Count", "data": data_emoji}, {"seriesname": "Text Length",
                                                                               "data": data_text}]

    with open("compare_chart.json", 'w') as output:
        json.dump(response, output)
    print("compare_chart finished after %s seconds" % (time.time() - start_time))


compare_chart()
