import json
import operator
import time

import pycountry
from Back.Utilities import db_handler as dbfile


def top_ten_total():
    start_time = time.time()
    print("top_ten_total starting")
    country_values = {'ZW': 0, 'ZM': 0, 'ZA': 0, 'YT': 0, 'YE': 0, 'WS': 0, 'WF': 0, 'VU': 0, 'VN': 0, 'VI': 0, 'VG': 0,
                      'VE': 0, 'VC': 0, 'VA': 0, 'UZ': 0, 'UY': 0, 'US': 0, 'UM': 0, 'UG': 0, 'UA': 0, 'TZ': 0, 'TW': 0,
                      'TV': 0, 'TT': 0, 'TR': 0, 'TO': 0, 'TN': 0, 'TM': 0, 'TL': 0, 'TK': 0, 'TJ': 0, 'TH': 0, 'TG': 0,
                      'TF': 0, 'TD': 0, 'TC': 0, 'SZ': 0, 'SY': 0, 'SX': 0, 'SV': 0, 'ST': 0, 'SS': 0, 'SR': 0, 'SO': 0,
                      'SN': 0, 'SM': 0, 'SL': 0, 'SK': 0, 'SJ': 0, 'SI': 0, 'SH': 0, 'SG': 0, 'SE': 0, 'SD': 0, 'SC': 0,
                      'SB': 0, 'SA': 0, 'RW': 0, 'RU': 0, 'RS': 0, 'RO': 0, 'RE': 0, 'QA': 0, 'PY': 0, 'PW': 0, 'PT': 0,
                      'PS': 0, 'PR': 0, 'PN': 0, 'PM': 0, 'PL': 0, 'PK': 0, 'PH': 0, 'PG': 0, 'PF': 0, 'PE': 0, 'PA': 0,
                      'OM': 0, 'NZ': 0, 'NU': 0, 'NR': 0, 'NP': 0, 'NO': 0, 'NL': 0, 'NI': 0, 'NG': 0, 'NF': 0, 'NE': 0,
                      'NC': 0, 'NA': 0, 'MZ': 0, 'MY': 0, 'MX': 0, 'MW': 0, 'MV': 0, 'MU': 0, 'MT': 0, 'MS': 0, 'MR': 0,
                      'MQ': 0, 'MP': 0, 'MO': 0, 'MN': 0, 'MM': 0, 'ML': 0, 'MK': 0, 'MH': 0, 'MG': 0, 'MF': 0, 'ME': 0,
                      'MD': 0, 'MC': 0, 'MA': 0, 'LY': 0, 'LV': 0, 'LU': 0, 'LT': 0, 'LS': 0, 'LR': 0, 'LK': 0, 'LI': 0,
                      'LC': 0, 'LB': 0, 'LA': 0, 'KZ': 0, 'KY': 0, 'KW': 0, 'KR': 0, 'KP': 0, 'KN': 0, 'KM': 0, 'KI': 0,
                      'KH': 0, 'KG': 0, 'KE': 0, 'JP': 0, 'JO': 0, 'JM': 0, 'JE': 0, 'IT': 0, 'IS': 0, 'IR': 0, 'IQ': 0,
                      'IO': 0, 'IN': 0, 'IM': 0, 'IL': 0, 'IE': 0, 'ID': 0, 'HU': 0, 'HT': 0, 'HR': 0, 'HN': 0, 'HM': 0,
                      'HK': 0, 'GY': 0, 'GW': 0, 'GU': 0, 'GT': 0, 'GS': 0, 'GR': 0, 'GQ': 0, 'GP': 0, 'GN': 0, 'GM': 0,
                      'GL': 0, 'GI': 0, 'GH': 0, 'GG': 0, 'GF': 0, 'GE': 0, 'GD': 0, 'GB': 0, 'GA': 0, 'FR': 0, 'FO': 0,
                      'FM': 0, 'FK': 0, 'FJ': 0, 'FI': 0, 'ET': 0, 'ES': 0, 'ER': 0, 'EH': 0, 'EG': 0, 'EE': 0, 'EC': 0,
                      'DZ': 0, 'DO': 0, 'DM': 0, 'DK': 0, 'DJ': 0, 'DE': 0, 'CZ': 0, 'CY': 0, 'CX': 0, 'CW': 0, 'CV': 0,
                      'CU': 0, 'CR': 0, 'CO': 0, 'CN': 0, 'CM': 0, 'CL': 0, 'CK': 0, 'CI': 0, 'CH': 0, 'CG': 0, 'CF': 0,
                      'CD': 0, 'CC': 0, 'CA': 0, 'BZ': 0, 'BY': 0, 'BW': 0, 'BV': 0, 'BT': 0, 'BS': 0, 'BR': 0, 'BQ': 0,
                      'BO': 0, 'BN': 0, 'BM': 0, 'BL': 0, 'BJ': 0, 'BI': 0, 'BH': 0, 'BG': 0, 'BF': 0, 'BE': 0, 'BD': 0,
                      'BB': 0, 'BA': 0, 'AZ': 0, 'AX': 0, 'AW': 0, 'AU': 0, 'AT': 0, 'AS': 0, 'AR': 0, 'AQ': 0, 'AO': 0,
                      'AM': 0, 'AL': 0, 'AI': 0, 'AG': 0, 'AF': 0, 'AE': 0, 'AD': 0}
    for doc in dbfile.new_emoji_collection.find({"place": {"$ne": None}}):
        if doc["place"]["country_code"] not in ['', 'XK']:
            country_values[doc["place"]["country_code"]] += 1

    top_ten = list(sorted(country_values.items(), key=operator.itemgetter(1), reverse=True)[:10])
    new_list = [{'label': str(pycountry.countries.get(alpha_2=item[0]).name), 'value': str(item[1])} for
                item in
                top_ten]
    with open('top_ten_total.json', 'w') as output:
            json.dump({'result': new_list}, output)
    print("top_ten_total finished after %s seconds" % (time.time() - start_time))

