import datetime
import operator
import time
from Back.Utilities import db_handler as dbfile
import json


def most_common_emoji():
    emojis = {'U0001f004': 0, 'U0001f0cf': 0, 'U0001f170': 0, 'U0001f171': 0, 'U0001f17e': 0, 'U0001f17f': 0,
              'U0001f18e': 0,
              'U0001f191': 0, 'U0001f192': 0, 'U0001f193': 0, 'U0001f194': 0, 'U0001f195': 0, 'U0001f196': 0,
              'U0001f197': 0,
              'U0001f198': 0, 'U0001f199': 0, 'U0001f19a': 0, 'U0001f1e8': 0, 'U0001f1e9': 0, 'U0001f1ea': 0,
              'U0001f1eb': 0,
              'U0001f1ec': 0, 'U0001f1ee': 0, 'U0001f1ef': 0, 'U0001f1f0': 0, 'U0001f1f7': 0, 'U0001f1fa': 0,
              'U0001f201': 0,
              'U0001f202': 0, 'U0001f21a': 0, 'U0001f22f': 0, 'U0001f232': 0, 'U0001f233': 0, 'U0001f234': 0,
              'U0001f235': 0,
              'U0001f236': 0, 'U0001f237': 0, 'U0001f238': 0, 'U0001f239': 0, 'U0001f23a': 0, 'U0001f250': 0,
              'U0001f251': 0,
              'U0001f300': 0, 'U0001f301': 0, 'U0001f302': 0, 'U0001f303': 0, 'U0001f304': 0, 'U0001f305': 0,
              'U0001f306': 0,
              'U0001f307': 0, 'U0001f308': 0, 'U0001f309': 0, 'U0001f30a': 0, 'U0001f30b': 0, 'U0001f30c': 0,
              'U0001f30f': 0,
              'U0001f311': 0, 'U0001f313': 0, 'U0001f314': 0, 'U0001f315': 0, 'U0001f319': 0, 'U0001f31b': 0,
              'U0001f31f': 0,
              'U0001f320': 0, 'U0001f330': 0, 'U0001f331': 0, 'U0001f334': 0, 'U0001f335': 0, 'U0001f337': 0,
              'U0001f338': 0,
              'U0001f339': 0, 'U0001f33a': 0, 'U0001f33b': 0, 'U0001f33c': 0, 'U0001f33d': 0, 'U0001f33e': 0,
              'U0001f33f': 0,
              'U0001f340': 0, 'U0001f341': 0, 'U0001f342': 0, 'U0001f343': 0, 'U0001f344': 0, 'U0001f345': 0,
              'U0001f346': 0,
              'U0001f347': 0, 'U0001f348': 0, 'U0001f349': 0, 'U0001f34a': 0, 'U0001f34c': 0, 'U0001f34d': 0,
              'U0001f34e': 0,
              'U0001f34f': 0, 'U0001f351': 0, 'U0001f352': 0, 'U0001f353': 0, 'U0001f354': 0, 'U0001f355': 0,
              'U0001f356': 0,
              'U0001f357': 0, 'U0001f358': 0, 'U0001f359': 0, 'U0001f35a': 0, 'U0001f35b': 0, 'U0001f35c': 0,
              'U0001f35d': 0,
              'U0001f35e': 0, 'U0001f35f': 0, 'U0001f360': 0, 'U0001f361': 0, 'U0001f362': 0, 'U0001f363': 0,
              'U0001f364': 0,
              'U0001f365': 0, 'U0001f366': 0, 'U0001f367': 0, 'U0001f368': 0, 'U0001f369': 0, 'U0001f36a': 0,
              'U0001f36b': 0,
              'U0001f36c': 0, 'U0001f36d': 0, 'U0001f36e': 0, 'U0001f36f': 0, 'U0001f370': 0, 'U0001f371': 0,
              'U0001f372': 0,
              'U0001f373': 0, 'U0001f374': 0, 'U0001f375': 0, 'U0001f376': 0, 'U0001f377': 0, 'U0001f378': 0,
              'U0001f379': 0,
              'U0001f37a': 0, 'U0001f37b': 0, 'U0001f380': 0, 'U0001f381': 0, 'U0001f382': 0, 'U0001f383': 0,
              'U0001f384': 0,
              'U0001f385': 0, 'U0001f386': 0, 'U0001f387': 0, 'U0001f388': 0, 'U0001f389': 0, 'U0001f38a': 0,
              'U0001f38b': 0,
              'U0001f38c': 0, 'U0001f38d': 0, 'U0001f38e': 0, 'U0001f38f': 0, 'U0001f390': 0, 'U0001f391': 0,
              'U0001f392': 0,
              'U0001f393': 0, 'U0001f3a0': 0, 'U0001f3a1': 0, 'U0001f3a2': 0, 'U0001f3a3': 0, 'U0001f3a4': 0,
              'U0001f3a5': 0,
              'U0001f3a6': 0, 'U0001f3a7': 0, 'U0001f3a8': 0, 'U0001f3a9': 0, 'U0001f3aa': 0, 'U0001f3ab': 0,
              'U0001f3ac': 0,
              'U0001f3ad': 0, 'U0001f3ae': 0, 'U0001f3af': 0, 'U0001f3b0': 0, 'U0001f3b1': 0, 'U0001f3b2': 0,
              'U0001f3b3': 0,
              'U0001f3b4': 0, 'U0001f3b5': 0, 'U0001f3b6': 0, 'U0001f3b7': 0, 'U0001f3b8': 0, 'U0001f3b9': 0,
              'U0001f3ba': 0,
              'U0001f3bb': 0, 'U0001f3bc': 0, 'U0001f3bd': 0, 'U0001f3be': 0, 'U0001f3bf': 0, 'U0001f3c0': 0,
              'U0001f3c1': 0,
              'U0001f3c2': 0, 'U0001f3c3': 0, 'U0001f3c4': 0, 'U0001f3c6': 0, 'U0001f3c8': 0, 'U0001f3ca': 0,
              'U0001f3e0': 0,
              'U0001f3e1': 0, 'U0001f3e2': 0, 'U0001f3e3': 0, 'U0001f3e5': 0, 'U0001f3e6': 0, 'U0001f3e7': 0,
              'U0001f3e8': 0,
              'U0001f3e9': 0, 'U0001f3ea': 0, 'U0001f3eb': 0, 'U0001f3ec': 0, 'U0001f3ed': 0, 'U0001f3ee': 0,
              'U0001f3ef': 0,
              'U0001f3f0': 0, 'U0001f40c': 0, 'U0001f40d': 0, 'U0001f40e': 0, 'U0001f411': 0, 'U0001f412': 0,
              'U0001f414': 0,
              'U0001f417': 0, 'U0001f418': 0, 'U0001f419': 0, 'U0001f41a': 0, 'U0001f41b': 0, 'U0001f41c': 0,
              'U0001f41d': 0,
              'U0001f41e': 0, 'U0001f41f': 0, 'U0001f420': 0, 'U0001f421': 0, 'U0001f422': 0, 'U0001f423': 0,
              'U0001f424': 0,
              'U0001f425': 0, 'U0001f426': 0, 'U0001f427': 0, 'U0001f428': 0, 'U0001f429': 0, 'U0001f42b': 0,
              'U0001f42c': 0,
              'U0001f42d': 0, 'U0001f42e': 0, 'U0001f42f': 0, 'U0001f430': 0, 'U0001f431': 0, 'U0001f432': 0,
              'U0001f433': 0,
              'U0001f434': 0, 'U0001f435': 0, 'U0001f436': 0, 'U0001f437': 0, 'U0001f438': 0, 'U0001f439': 0,
              'U0001f43a': 0,
              'U0001f43b': 0, 'U0001f43c': 0, 'U0001f43d': 0, 'U0001f43e': 0, 'U0001f440': 0, 'U0001f442': 0,
              'U0001f443': 0,
              'U0001f444': 0, 'U0001f445': 0, 'U0001f446': 0, 'U0001f447': 0, 'U0001f448': 0, 'U0001f449': 0,
              'U0001f44a': 0,
              'U0001f44b': 0, 'U0001f44c': 0, 'U0001f44d': 0, 'U0001f44e': 0, 'U0001f44f': 0, 'U0001f450': 0,
              'U0001f451': 0,
              'U0001f452': 0, 'U0001f453': 0, 'U0001f454': 0, 'U0001f455': 0, 'U0001f456': 0, 'U0001f457': 0,
              'U0001f458': 0,
              'U0001f459': 0, 'U0001f45a': 0, 'U0001f45b': 0, 'U0001f45c': 0, 'U0001f45d': 0, 'U0001f45e': 0,
              'U0001f45f': 0,
              'U0001f460': 0, 'U0001f461': 0, 'U0001f462': 0, 'U0001f463': 0, 'U0001f464': 0, 'U0001f466': 0,
              'U0001f467': 0,
              'U0001f468': 0, 'U0001f469': 0, 'U0001f46a': 0, 'U0001f46b': 0, 'U0001f46e': 0, 'U0001f46f': 0,
              'U0001f470': 0,
              'U0001f471': 0, 'U0001f472': 0, 'U0001f473': 0, 'U0001f474': 0, 'U0001f475': 0, 'U0001f476': 0,
              'U0001f477': 0,
              'U0001f478': 0, 'U0001f479': 0, 'U0001f47a': 0, 'U0001f47b': 0, 'U0001f47c': 0, 'U0001f47d': 0,
              'U0001f47e': 0,
              'U0001f47f': 0, 'U0001f480': 0, 'U0001f481': 0, 'U0001f482': 0, 'U0001f483': 0, 'U0001f484': 0,
              'U0001f485': 0,
              'U0001f486': 0, 'U0001f487': 0, 'U0001f488': 0, 'U0001f489': 0, 'U0001f48a': 0, 'U0001f48b': 0,
              'U0001f48c': 0,
              'U0001f48d': 0, 'U0001f48e': 0, 'U0001f48f': 0, 'U0001f490': 0, 'U0001f491': 0, 'U0001f492': 0,
              'U0001f493': 0,
              'U0001f494': 0, 'U0001f495': 0, 'U0001f496': 0, 'U0001f497': 0, 'U0001f498': 0, 'U0001f499': 0,
              'U0001f49a': 0,
              'U0001f49b': 0, 'U0001f49c': 0, 'U0001f49d': 0, 'U0001f49e': 0, 'U0001f49f': 0, 'U0001f4a0': 0,
              'U0001f4a1': 0,
              'U0001f4a2': 0, 'U0001f4a3': 0, 'U0001f4a4': 0, 'U0001f4a5': 0, 'U0001f4a6': 0, 'U0001f4a7': 0,
              'U0001f4a8': 0,
              'U0001f4a9': 0, 'U0001f4aa': 0, 'U0001f4ab': 0, 'U0001f4ac': 0, 'U0001f4ae': 0, 'U0001f4af': 0,
              'U0001f4b0': 0,
              'U0001f4b1': 0, 'U0001f4b2': 0, 'U0001f4b3': 0, 'U0001f4b4': 0, 'U0001f4b5': 0, 'U0001f4b8': 0,
              'U0001f4b9': 0,
              'U0001f4ba': 0, 'U0001f4bb': 0, 'U0001f4bc': 0, 'U0001f4bd': 0, 'U0001f4be': 0, 'U0001f4bf': 0,
              'U0001f4c0': 0,
              'U0001f4c1': 0, 'U0001f4c2': 0, 'U0001f4c3': 0, 'U0001f4c4': 0, 'U0001f4c5': 0, 'U0001f4c6': 0,
              'U0001f4c7': 0,
              'U0001f4c8': 0, 'U0001f4c9': 0, 'U0001f4ca': 0, 'U0001f4cb': 0, 'U0001f4cc': 0, 'U0001f4cd': 0,
              'U0001f4ce': 0,
              'U0001f4cf': 0, 'U0001f4d0': 0, 'U0001f4d1': 0, 'U0001f4d2': 0, 'U0001f4d3': 0, 'U0001f4d4': 0,
              'U0001f4d5': 0,
              'U0001f4d6': 0, 'U0001f4d7': 0, 'U0001f4d8': 0, 'U0001f4d9': 0, 'U0001f4da': 0, 'U0001f4db': 0,
              'U0001f4dc': 0,
              'U0001f4dd': 0, 'U0001f4de': 0, 'U0001f4df': 0, 'U0001f4e0': 0, 'U0001f4e1': 0, 'U0001f4e2': 0,
              'U0001f4e3': 0,
              'U0001f4e4': 0, 'U0001f4e5': 0, 'U0001f4e6': 0, 'U0001f4e7': 0, 'U0001f4e8': 0, 'U0001f4e9': 0,
              'U0001f4ea': 0,
              'U0001f4eb': 0, 'U0001f4ee': 0, 'U0001f4f0': 0, 'U0001f4f1': 0, 'U0001f4f2': 0, 'U0001f4f3': 0,
              'U0001f4f4': 0,
              'U0001f4f6': 0, 'U0001f4f7': 0, 'U0001f4f9': 0, 'U0001f4fa': 0, 'U0001f4fb': 0, 'U0001f4fc': 0,
              'U0001f503': 0,
              'U0001f50a': 0, 'U0001f50b': 0, 'U0001f50c': 0, 'U0001f50d': 0, 'U0001f50e': 0, 'U0001f50f': 0,
              'U0001f510': 0,
              'U0001f511': 0, 'U0001f512': 0, 'U0001f513': 0, 'U0001f514': 0, 'U0001f516': 0, 'U0001f517': 0,
              'U0001f518': 0,
              'U0001f519': 0, 'U0001f51a': 0, 'U0001f51b': 0, 'U0001f51c': 0, 'U0001f51d': 0, 'U0001f51e': 0,
              'U0001f51f': 0,
              'U0001f520': 0, 'U0001f521': 0, 'U0001f522': 0, 'U0001f523': 0, 'U0001f524': 0, 'U0001f525': 0,
              'U0001f526': 0,
              'U0001f527': 0, 'U0001f528': 0, 'U0001f529': 0, 'U0001f52a': 0, 'U0001f52b': 0, 'U0001f52e': 0,
              'U0001f52f': 0,
              'U0001f530': 0, 'U0001f531': 0, 'U0001f532': 0, 'U0001f533': 0, 'U0001f534': 0, 'U0001f535': 0,
              'U0001f536': 0,
              'U0001f537': 0, 'U0001f538': 0, 'U0001f539': 0, 'U0001f53a': 0, 'U0001f53b': 0, 'U0001f53c': 0,
              'U0001f53d': 0,
              'U0001f550': 0, 'U0001f551': 0, 'U0001f552': 0, 'U0001f553': 0, 'U0001f554': 0, 'U0001f555': 0,
              'U0001f556': 0,
              'U0001f557': 0, 'U0001f558': 0, 'U0001f559': 0, 'U0001f55a': 0, 'U0001f55b': 0, 'U0001f5fb': 0,
              'U0001f5fc': 0,
              'U0001f5fd': 0, 'U0001f5fe': 0, 'U0001f5ff': 0, 'U0001f601': 0, 'U0001f602': 0, 'U0001f603': 0,
              'U0001f604': 0,
              'U0001f605': 0, 'U0001f606': 0, 'U0001f609': 0, 'U0001f60a': 0, 'U0001f60b': 0, 'U0001f60c': 0,
              'U0001f60d': 0,
              'U0001f60f': 0, 'U0001f612': 0, 'U0001f613': 0, 'U0001f614': 0, 'U0001f616': 0, 'U0001f618': 0,
              'U0001f61a': 0,
              'U0001f61c': 0, 'U0001f61d': 0, 'U0001f61e': 0, 'U0001f620': 0, 'U0001f621': 0, 'U0001f622': 0,
              'U0001f623': 0,
              'U0001f624': 0, 'U0001f625': 0, 'U0001f628': 0, 'U0001f629': 0, 'U0001f62a': 0, 'U0001f62b': 0,
              'U0001f62d': 0,
              'U0001f630': 0, 'U0001f631': 0, 'U0001f632': 0, 'U0001f633': 0, 'U0001f635': 0, 'U0001f637': 0,
              'U0001f638': 0,
              'U0001f639': 0, 'U0001f63a': 0, 'U0001f63b': 0, 'U0001f63c': 0, 'U0001f63d': 0, 'U0001f63e': 0,
              'U0001f63f': 0,
              'U0001f640': 0, 'U0001f645': 0, 'U0001f646': 0, 'U0001f647': 0, 'U0001f648': 0, 'U0001f649': 0,
              'U0001f64a': 0,
              'U0001f64b': 0, 'U0001f64c': 0, 'U0001f64d': 0, 'U0001f64e': 0, 'U0001f64f': 0, 'U0001f680': 0,
              'U0001f683': 0,
              'U0001f684': 0, 'U0001f685': 0, 'U0001f687': 0, 'U0001f689': 0, 'U0001f68c': 0, 'U0001f68f': 0,
              'U0001f691': 0,
              'U0001f692': 0, 'U0001f693': 0, 'U0001f695': 0, 'U0001f697': 0, 'U0001f699': 0, 'U0001f69a': 0,
              'U0001f6a2': 0,
              'U0001f6a4': 0, 'U0001f6a5': 0, 'U0001f6a7': 0, 'U0001f6a8': 0, 'U0001f6a9': 0, 'U0001f6aa': 0,
              'U0001f6ab': 0,
              'U0001f6ac': 0, 'U0001f6ad': 0, 'U0001f6b2': 0, 'U0001f6b6': 0, 'U0001f6b9': 0, 'U0001f6ba': 0,
              'U0001f6bb': 0,
              'U0001f6bc': 0, 'U0001f6bd': 0, 'U0001f6be': 0, 'U0001f6c0': 0}
    for doc in dbfile.new_emoji_collection.find(
            {'created_at': {'$gt': datetime.datetime.now() - datetime.timedelta(hours=24)}}):
        for emoji in doc['emoji_list']:
            if emoji in emojis.keys():
                emojis[emoji] += 1

    return max(emojis.items(), key=operator.itemgetter(1))[0]


def emoji_text_ratio():
    pipeline = [{
        "$group": {
            "_id": None,
            "avg_emoji": {"$avg": "$emoji_count"},
            "avg_text": {"$avg": "$text_length"}
        }
    }]
    result = list(dbfile.new_emoji_collection.aggregate(pipeline))
    res = {'avg_emoji': "%.3f" % result[0]['avg_emoji'], 'avg_text': "%.3f" % result[0]['avg_text'],
           'ratio': "%.3f" % (result[0]['avg_text'] / result[0]['avg_emoji'])}
    return res


def build_dashboard():
    emoji = '\\'+most_common_emoji()
    start_time = time.time()
    print("Building dashboard")
    with open('dashboard.json', 'w') as output:
        json.dump({'total_tweets': "{:,}".format(dbfile.MY_DB.command('dbstats')['objects']),
                   'total_geo': "{:,}".format(dbfile.geo_collection.count()),
                   'most_common': bytes(emoji.encode('unicode-escape')).decode('unicode-escape'),
                   'ratios': emoji_text_ratio()
                   }, output)
    print("Built dashboard after %s seconds" % (time.time() - start_time))


build_dashboard()