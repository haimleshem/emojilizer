import datetime
import json
import time

from Back.Utilities import db_handler as dbfile


def four_weeks():
    start_time = time.time()
    print("four_weeks starting")
    today = datetime.datetime.today()
    last_week = today - datetime.timedelta(days=datetime.datetime.today().weekday(), weeks=1)
    last_two_weeks = today - datetime.timedelta(days=datetime.datetime.today().weekday(), weeks=2)
    last_three_weeks = today - datetime.timedelta(days=datetime.datetime.today().weekday(), weeks=3)
    last_four_weeks = today - datetime.timedelta(days=datetime.datetime.today().weekday(), weeks=4)
    result = list()
    times = [today, last_week, last_two_weeks, last_three_weeks, last_four_weeks]
    for i in range(1, 5):
        pipeline = [
            {"$match": {"created_at": {"$gt": times[i], "$lt": times[i - 1]}}},
            {"$group": {
                "_id": None,
                "value": {"$sum": "$emoji_count"},
            }
            }]

        result.append(list(dbfile.new_emoji_collection.aggregate(pipeline)))
    arr = []
    for item in result:
        if len(item) != 0:
            arr.append({"value": item[0]['value']})

    with open('four_weeks.json', 'w') as output:
        json.dump({'result': [{'seriesName': '', 'data': arr}]}, output)
    print("four_weeks finished after %s seconds" % (time.time() - start_time))

four_weeks()