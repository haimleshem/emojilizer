from geojson import Feature, Point
from Back.Utilities import db_handler


def makeGeoJsonPoints(jsonDict):
    myFeature = Feature(
        geometry=Point((float(jsonDict["place"]["bounding_box"]['coordinates'][0][0][0]),
                        float(jsonDict["place"]["bounding_box"]['coordinates'][0][0][1]))),
        properties={"_id": jsonDict["_id"],
                    "description": jsonDict["text"],
                    "country_code": jsonDict["place"]["country_code"],
                    "emoji_list": jsonDict['emoji_list'],
                    "created_at": jsonDict['created_at']
                    }

    )

    return myFeature


def makeGeojson(jsonDict):
    try:
        feat = makeGeoJsonPoints(jsonDict)
        # print('added geo')
        db_handler.addGeoListing(feat)
    except Exception as error:
        print("Unable to write %s error" % error)
