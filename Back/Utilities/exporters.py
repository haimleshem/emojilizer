import json
import os
import pycountry
from Back.Utilities import db_handler as dbfile


def export_geojson():
    cursor = dbfile.geo_collection.find({})
    with open('geo.geojson', 'w') as geofile:
        geofile.close()
    with open('geo.geojson', 'r+') as file:
        file.write('''{"type": "FeatureCollection", "features":''')
        file.write('[')
        for doc in cursor:
            del doc["_id"]
            file.write(json.dumps(doc))
            file.write(',')
        file.close()

    with open('geo.geojson', 'r+b')as file2:
        file2.seek(-1, os.SEEK_END)
        file2.truncate()
        file2.close()

    with open('geo.geojson', 'r+') as file3:
        file3.seek(0, os.SEEK_END)
        file3.write(']}')

export_geojson()
