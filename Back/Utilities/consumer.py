import re

import pika
import json
import json_handler, geojson_handler, no_emoji_json_handler

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='tweets')


def callback(ch, method, properties, response):
    try:
        response = json.loads(response)
        # print(response)
        formatted_text = str(response["text"].encode('unicode-escape'))
        # print('formatted')
        emoji_reg = re.compile(r'(U0001f([a-z]|[0-9])([a-z]|[0-9])([a-z]|[0-9]))')
        if emoji_reg.search(formatted_text):
            # print('in if')
            emojis = [i.group() for i in re.finditer(emoji_reg, formatted_text)]
            # print(emojis)
            jsonDict = json_handler.dumpJson(response, emojis)
            if jsonDict['place']:
                geojson_handler.makeGeojson(jsonDict)
        else:
            no_emoji_json_handler.makeJson(response)

    except BaseException as e:
        if str(e) != "'place'" and str(e) != "'text'":
            print('Error on data: %s' % str(e))


channel.basic_consume(callback, queue='tweets', no_ack=True)
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
