import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('127.0.0.1'))
channel = connection.channel()
channel.queue_declare(queue='tweets')

print("Connection established - queue created.")


def send_tweet(tweet):
    try:
        channel.basic_publish(exchange='',
                              routing_key='tweets',
                              body=tweet)
    except Exception as e:
        print(e)
        pass

