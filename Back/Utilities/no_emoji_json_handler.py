from Back.Utilities import db_handler


def makeJson(jsonObject):
    jsonDict = dict()
    jsonDict["_id"] = jsonObject["id_str"]
    jsonDict["created_at"] = jsonObject["created_at"]
    jsonDict["text"] = jsonObject["text"]
    jsonDict["user"] = {"id": jsonObject["user"]["id_str"],
                        "verified": jsonObject["user"]["verified"],
                        "followers_count": jsonObject["user"]["followers_count"],
                        "friends_count": jsonObject["user"]["friends_count"],
                        "statuses_count": jsonObject["user"]["statuses_count"],
                        "created_at": jsonObject["user"]["created_at"],
                        "lang": jsonObject["user"]["lang"],
                        "user_favorite_count": jsonObject["user"]["favourites_count"]
                        }
    jsonDict["place"] = jsonObject["place"]
    jsonDict["tweet_quote_count"] = jsonObject["quote_count"]
    jsonDict["tweet_reply_count"] = jsonObject["reply_count"]
    jsonDict["tweet_retweet_count"] = jsonObject["retweet_count"]
    jsonDict["tweet_favourite_count"] = jsonObject["favorite_count"]
    jsonDict["text_length"] = len(str(jsonObject["text"].encode('unicode-escape')))

    db_handler.addNoEmojiTweet(jsonDict)
