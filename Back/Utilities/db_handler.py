import pymongo

CLIENT = pymongo.MongoClient("mongodb://127.0.0.1:27017/")
MY_DB = CLIENT["Tweets"]
emoji_collection = MY_DB['tweets']
geo_collection = MY_DB['tweets_geo']
no_emoji_collection = MY_DB['tweets_no_emoji']
new_emoji_collection = MY_DB['tweets_emoji']
i = 1


def addGeoListing(jsonDict):
    global i
    geo_collection.insert_one(jsonDict.copy())
    print("Tweet #" + str(i) + " with geographic information appended successfully")
    i += 1


def addNoEmojiTweet(jsonDict):
    no_emoji_collection.insert_one(jsonDict.copy())


def addNewEmojiTweet(jsonDict):
    new_emoji_collection.insert_one(jsonDict.copy())
